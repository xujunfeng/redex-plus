#lang scribble/manual

@(define coq tt)
@(define beluga tt)

@title{redex-plus}

This document provides an overview of the Redex subset implemented by Redex-plus.
The goal is that every valid Redex-plus form should also be valid in Redex.

I will try my best to keep this document up-to-date with the library.



@section{Overview}

Redex-plus is an extension of Redex.
Given a Redex model, Redex-plus does the following things:

@itemize[
@item{
Checks the Redex model and warns the user of certain potential errors,
including language and judgement-form definitions without 'base cases',
and potential misspellings.
}

@item{
Reëxport all Redex definitions so the model can be used
as if it is defined in Redex.
}

@item{
When a proof assistant backend is specified,
Redex-plus translates the Redex model into proof assistant code,
and writes the translated code to @tt{stdout}.
}
]

An objective of Redex-plus is to encode variable binding information,
specified by the use of @racket[#;binding-forms] in Redex,
precisely in the proof assistant code generated.
In this way, the user can implement proper variable binding in their language model
without writing proof assistant-specific boilerplates.

Currently, Redex-plus supports Coq and Beluga backends.
For Coq, the @(hyperlink "https://github.com/lpw25/shifted-names" "shifted-name")
library is used to encode binding and handle substitutions.
For Beluga, the idiomatic HOAS encoding is used. 



@section{Configuration}

@defform/subs[(use-backend backend-name)
              ([backend-name
                redex
                beluga
                coq])]{
 Selects the backend to target.
 The choice of backend determines how the language definitions will be implemented.

 If the backend is set to Redex, then no proof assistant code will be generated.
 If the backend is set to a proof assistant, then the model will be transtaled into
 proof assistant code and written to @tt{stdoit}.
 Either way, the Redex model will be defined and available in the Racket code.

 If @racket[use-backend] is not present,
 then the default Redex backend will be used.
}



@section{Pattern}

Redex-plus supports a small but useful subset of Redex's pattern language.
In general, only patterns that can be represented in proof assistants are supported.

@(racketgrammar*
   [pattern natural
            variable 
            symbol
            (pattern ...)
            ])

@itemize[

@item{
A @racket[natural] is a natural number represented as Peano numeral in proof assistants.
It is translated into the built-in @coq{nat} type in Coq,
and into a custom @beluga{nat} type provided in the prelude in Beluga.
}

@item{
A @racket[variable] is a varible that may become bound if a @racket[binding-form] is present.
It is translated differently when targeting different proof assistants
and using different variable binding representations.
Currently, the following representations are used:

@itemize[
@item{
In Coq, @racket[variable]s in a binding context are represented as shifted-name @coq{var}s,
which, depending on the exact term, can be either free or bound.

@racket[variable]s outside binding contexts are represented as shifted-name @coq{name}s,
which are strings indexed with a natural number.
The index is useful when working with different bound variables with the same name.
But for our purpose, all @coq{name}s will have an index of zero.

See the section on shifted-names for more information.
}

@item{
In Beluga where HOAS representations for bindings are used,
a binding context is represented as a function,
and a @racket[variable] is bound by that function.

@racket[variable]s outside of binding contexts are not supported,
and any models using them need to be rewritten.
}
]

}

@item{
A @racket[symbol] can either be:
@itemize[
@item{
The name of a non-terminal in the same language.
In this case, it matches a term of that non-terminal.
In translated code, this would translate to one constructor argument
for the pattern it belongs to.

Notice that in Redex, you may suffix non-terminal patterns to specify
that non-terminal patterns with the same suffix must be the same.
This is not supported by Redex-plus,
as this is difficult to translate to proof assistants.
}

@item{
A literal literal that has only syntactic meanings and must be matched verbatim.
Such literals will not be included in the translated code.
}
]
}

@item{
Patterns may also be sequences of sub-patterns.
There are no restrictions regarding nesting or placement of literals and other sub-patterns.
When translated to proof assistant code,
all structural information except the order of non-terminal sub-patterns
(which are translated to constructor arguments)
are removed.
}

]


@section{Language}

@defform/subs[#:literals (::= shadow nothing symbol)
              (define-language lang-name 
                non-terminal-def ...
                maybe-binding-spec)
              ([non-terminal-def (non-terminal-name ::= pattern ...+)
                                 (non-terminal-name pattern ...+)
                                 ((non-terminal-name) pattern ...+)]
               [maybe-binding-spec (code:line)
                                   (code:line #:binding-forms binding-pattern ...)]
               [binding-pattern
                pattern
                (code:line binding-pattern #:exports beta)
                ]
	       [beta nothing
	             symbol])]{
 Non-terminal aliasing is not implemented yet, but I will address this in the future.

 When using the Redex backend,
 @racket[define-language] performs additional checks on the language definition provided,
 to find non-terminal definitions without base cases (i.e. not well-founded),
 and detect possible misspellings.

 When using the @racket[coq] backend, in addition to the aforementioned checks,
 each non-terminal will be translated into one Type definition in Coq.
 The resultant Coq definitions will be written to @racket{current-output-port}.
 
 Binding forms, if present, will be translated using a de Bruijn-style implementation
 backed by the @(hyperlink "https://github.com/lpw25/shifted-names" "shifted-name")
 Coq library.

 When using binding forms, some restrictions apply:
 @itemize[
 @item{If a non-terminal has a constructor with a binding form,
   then the said non-terminal must have a constructor that takes nothing but a @racket[variable].}
 @item{If any non-terminal has a constructor with a binding form,
   then no other non-terminal can have constructors that takes only one variable.}]

 The first restriction is enforced because shifted-names require types with binding
 to have a unit morph, which will map any variable back to itself.
 If a non-terminal does not have a case for varibales, then such a unit morph cannot exist.
 The second restriction is enforced because the substitution process is recursive across
 all non-terminals in a language.
 If I want to replace a variable @racket[x] to a term @racket[e]
 which is an instance of a non-terminal @racket[nt], then what should happen when
 I run into the variable @racket[x] when the recursive procedure is running over
 another non-terminal @racket[nt2] is difficult to define:
 I cannot replace the variable, because it can only be replaced by a term of type @racket[nt2],
 while @racket[e] has type @racket[nt].
 But leaving the variable unchanged does not seem correct either.
}

@defform/subs[#:literals (::= ....)
              (define-extended-language extended-lang base-lang 
                non-terminal-def ...
                maybe-binding-spec)
              ([non-terminal-def (non-terminal-name ::= pattern-or-extension ...+)
                (non-terminal-name pattern-or-extension ...+)
                ((non-terminal-name) pattern-or-extension ...+)]
               [pattern-or-extension .... pattern]
               [maybe-binding-spec (code:line)
                (code:line #:binding-forms binding-pattern ...)])]{
 Extends an existing language definition.
}

@section{Judgement Form}

@defform/subs[#:literals (I O where where/hidden where/error
                            side-condition side-condition/hidden
                            etc.)
             (define-judgment-form language
               mode-spec
               contract-spec
               invariant-spec
               rule rule ...)
             ([mode-spec (code:line) (code:line #:mode (form-id pos-use ...))]
              [contract-spec (code:line #:contract (form-id pattern ...))]
              [invariant-spec (code:line)
                              (code:line #:inv term)]
              [pos-use I
                       O]
              [rule [premise
                     ... 
                     dashes rule-name
                     conclusion]
                    [conclusion 
                     premise 
                     ...
                     rule-name]]
              [conclusion (form-id pat/term ...)]
              [premise (code:line (judgment-form-id pat/term ...))]
              [rule-name (code:line)
                         string]
              [pat/term pattern term]
              [dashes ---
                      ----
                      -----
                      etc.])]{
 Also available as @racket[define-judgment-form].

 When using the Redex backend,
 @racket[define-judgement-form] performs additional checks on the judgement form definition provided,
 to find judgement form definitions without axioms (i.e. not well-founded),
 and detect possible misspellings.

 When using the @racket[coq] backend, in addition to the aformentioned checks,
 each judgement form will be translated into one Prop definition in Coq,
 where each rule would correspond to one constructor.
 The resultant Coq definitions will be written to @racket{current-output-port}.
                             
 @racket[contract-spec] is mandatory in Redex-plus although it is not required by Redex.
 I need @racket[contract-spec] to figure out how to translate pattern/terms
 in the judgement form bodies.
}

@section{Licence}

Much of this document's source is adapted from the Redex reference, which, as a part of Racket,
is available under the MIT license, produced below:

@verbatim{
Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
}

This document itself is available also under the MIT licence, as a part of Redex-plus.
