#lang racket

(require "./../redex-plus/util.rkt"
         "./../redex-plus/config.rkt")

(provide display-prelude

         define-type
         define-types

         define-proposition

         for-language

         encode-identifier
         )


(define current-language (make-parameter #f))

(define (replace-strings pairs str)
  (for/fold ([output str])
            ([replace pairs])
    (string-replace output (first replace) (~a (second replace)))))

(define (encode-identifier id)
  (string->symbol
   (replace-strings
    '(["-->" "_ReducesTo_"]
      ["->" "_arrow_"]
      ["=>" "_Arrow_"]
      ["<-" "_larrow_"]
      ["<=" "_lArrow_"]
      [":=" "_Assign_"]
      ["_" "__"]
      ["-" "_"]
      ["+" "_Plus_"]
      ["*" "_Star_"]
      ["/" "_Slash_"]
      ["&" "_And_"]
      ["^" "_Hat_"]
      ["@" "_At_"]
      ["$" "_Dollar_"]
      ["!" "_Exclamation_"]
      ["=" "_Equal_"]
      ["%" "_Percent_"]
      ["~" "_Tilde_"]
      ["?" "_Question_"]
      [">" "_Greater_"]
      ["<" "_Less_"]
      [":" "_Colon_"])
    (~a id))))


(define prelude-included? (make-parameter #f))

(define (display-prelude)
  (unless (prelude-included?)
    (displayln
     "
% Include a natural number definition:
nat : type.
o : nat.
s : nat → nat.

% And a boolean definition
bool : type.
true : bool.
false : bool.


% Boolean logic
rec b_and : [ ⊢ bool ] → [ ⊢ bool ] → [ ⊢ bool ] = fn b1 ⇒ fn b2 ⇒ case b1 of
  | [ ⊢ true ] ⇒ b2
  | [ ⊢ false ] ⇒ [ ⊢ false ];

rec b_or : [ ⊢ bool ] → [ ⊢ bool ] → [ ⊢ bool ] = fn b1 ⇒ fn b2 ⇒ case b1 of
  | [ ⊢ true ] ⇒ [ ⊢ true ]
  | [ ⊢ false ] ⇒ b2;

rec b_not : [ ⊢ bool ] → [ ⊢ bool ] = fn b ⇒ case b of
  | [ ⊢ true ] ⇒ [ ⊢ false ]
  | [ ⊢ false ] ⇒ [ ⊢ true ];


% Boolean equalities
rec b_eq : [ ⊢ bool ] → [ ⊢ bool ] → [ ⊢ bool ] = fn b1 ⇒ fn b2 ⇒ case (b1, b2) of
  | ([ ⊢ B1 ], [ ⊢ B1 ]) ⇒ [ ⊢ true ]
  | ([ ⊢ B1 ], [ ⊢ B2 ]) ⇒ [ ⊢ false ];

rec n_eq : [ ⊢ nat ] → [ ⊢ nat ] → [ ⊢ bool ] = fn n1 ⇒ fn n2 ⇒ case (n1, n2) of
  | ([ ⊢ o ], [ ⊢ o ]) ⇒ [ ⊢ true ]
  | ([ ⊢ s N1 ], [ ⊢ s N2 ]) ⇒ n_eq [ ⊢ N1 ] [ ⊢ N2]
  | ([ ⊢ s N1 ], [ ⊢ o ]) ⇒ [ ⊢ false ]
  | ([ ⊢ o ], [ ⊢ s N2 ]) ⇒ [ ⊢ false ];

")
    (prelude-included? #t)))


(define (implement-native-type type)
  (match type
    ['natural "nat"]
    ['integer
     (error 'translate-type
            "The Beluga backend does not support native integers")]
    [other
     (error 'define-type
            "Type '~a' is not natively implemented for the Beluga backend."
            other)]))


(define find-binding-in-current-definition (make-parameter (const '())))

(define (translate-term bound-variables quantified-variables term)
  (match term
    [bound
     #:when (and (member bound bound-variables)
                 (not (non-terminal-has-binding?
                       (current-language)
                       (canonical-name bound))))
     (~a (encode-identifier bound))]

    [bound
     #:when (member bound bound-variables)
     (for/fold ([bound-variable (~a (encode-identifier bound))])
               ([quantified-variable quantified-variables])
       (format "(~a ~a)"
               bound-variable

               (encode-identifier quantified-variable)))]
    [b #:when (equal? b prop-#t) "True"]
    [b #:when (equal? b prop-#f) "False"]
    [b #:when (equal? b bool-#t) "true"]
    [b #:when (equal? b bool-#f) "false"]
    ['#:any "_"]

    [`(#:under-binding ,variable ,bound)
     #:when (member bound bound-variables)
     (~a (encode-identifier bound))]

    [`(#:under-binding ,variable ,term)
     (format "(\\~a . ~a)"
             (encode-identifier variable)
             (translate-term bound-variables
                             (cons variable quantified-variables)
                             term))]

    [`(#:substitute ,term ,from ,to)
     ; TODO: make sure that substitution only happens
     ;       if term is surely bound under from
     (when (not (equal? (first ((find-binding-in-current-definition) term))
                        from))
       (error 'translate-term
              "The term '~a' does not necessarily contain the bound variable '~a'."
              term
              from))
     (format "(~a ~a)"
             (translate-term bound-variables quantified-variables term)
             (translate-term bound-variables quantified-variables to))]

    [`(,variable-constructor ,variable)
     #:when (equal? (binding-information-for-constructor-argument
                     (current-language)
                     variable-constructor
                     0)
                    'variable)
     (translate-term bound-variables quantified-variables variable)]

    [`(#:variable ,name)
     (~a (encode-identifier name))]

    [application
     #:when (list? application)
     (string-append
      "("
      (string-join (map (((curry translate-term)
                          bound-variables)
                         quantified-variables)
                        application))
      ")")]
    [constructor
     #:when (constructor-of-language? (current-language) constructor)
     (format "~a_~a"
             (encode-identifier (current-language))
             (encode-identifier constructor))]
    [atom (~a (encode-identifier atom))]))

(define (translate-type bound-variables type)
  (match type
    [`(#:typed-variable ,t) t]

    [`(#:with-binding ,bound-type ,result-type)
     (format "(~a → ~a)"
             (translate-type bound-variables bound-type)
             (translate-type bound-variables result-type))]
    ['#:Prop "type"]
    ['#:bool "bool"]
    [`(#:term ,term) (translate-term bound-variables '() term)]
    ['(bound)
     (error 'translate-type
            "Bound variables should be represented using HOAS when using the Beluga backend.")]
    [`(affected ,vts ,type)
     (let* ([nt-type-corresponding-to-bound-variable
             (match vts
               [`(variable) type]
               [`((#:typed-variable ,vt))
                (non-terminal-corresponding-to-variable-type
                 (current-language)
                 vt)]
               [_ (error 'define-type
                         "Multiple bindings in one term is not supported.")])]
            [bound-type (format "~a_~a"
                                (encode-identifier (current-language))
                                (encode-identifier nt-type-corresponding-to-bound-variable))]
            [affected-type (format "~a_~a"
                                   (encode-identifier (current-language))
                                   (encode-identifier type))])
       (format "(~a → ~a)" bound-type affected-type))]

    [application
     #:when (list? application)
     (string-append
      "("
      (string-join
       (for/list ([head/arg application])
         (translate-type bound-variables head/arg)))
      ")")]

    [native
     #:when (member native native-types)
     (implement-native-type type)]
    [bound
     #:when (member bound bound-variables)
     (~a (encode-identifier bound))]
    [_ (format "~a_~a"
               (encode-identifier (current-language))
               (encode-identifier type))]))


(define (define-type name constructors)
  (define-types
    (list name)
    (list constructors)))


(define (translate-list-of-argument-types arg-types)
  (string-join
   (map ((curry translate-type) (list)) arg-types)
   " → "))

(define (define-types names constructor-lists)
  (define type-declarations
    (string-join
     (for/list ([name names])
       (format "~a_~a : type."
               (encode-identifier (current-language))
               (encode-identifier name)))
     "\n"))

  (define constructors
    (string-join
     (append*
      (for/list ([name names]
                 [constructors constructor-lists])
        (for/list ([constructor constructors])
          (format "~a_~a : ~a ~a ~a."
                  (encode-identifier (current-language))
                  (encode-identifier (first constructor))
                  (translate-list-of-argument-types
                   (last constructor))
                  (if (empty? (last constructor))
                      ""
                      " → ")
                  (translate-type (list) name)))))
     "\n"))

  (string-append
   type-declarations
   "\n\n"
   constructors
   "\n\n"))


(define (define-proposition name arg-types constructors)
  (define proposition-declaration
    (format "~a_~a : ~a ~a type."
            (encode-identifier (current-language))
            (encode-identifier name)
            (translate-list-of-argument-types arg-types)
            (if (empty? arg-types)
                ""
                "→")))

  (define (derive-implicit-argument-types-in-term
           implicit-arguments
           term-type
           term)
    (match term
      [`(#:under-binding ,variable ,term)
       (for/list
           ([arg*arg-type (derive-implicit-argument-types-in-term
                           implicit-arguments
                           term-type
                           term)])
         (let ([arg (car arg*arg-type)]
               [arg-type (cdr arg*arg-type)])
           (if (member arg-type native-types)
               arg*arg-type
               ; We know that term-type must be the type with binding here
               (cons arg
                     `(#:with-binding ,term-type ,arg-type)))))]

      [`(#:substitute ,with-binding ,variable ,to)
       (append
        (derive-implicit-argument-types-in-term
         implicit-arguments
         term-type
         with-binding)
        (derive-implicit-argument-types-in-term
         implicit-arguments
         term-type
         to))]

      [`(,constructor-name ,constructor-arguments ...)
       (append* (for/list ([arg constructor-arguments]
                           [arg-type (filter
                                      (λ (t)
                                        (not (equal? t 'variable)))
                                      (argument-types-for-constructor
                                       (current-language)
                                       constructor-name))])
                  (derive-implicit-argument-types-in-term
                   implicit-arguments
                   arg-type
                   arg)))]
      [bound
       #:when (member bound implicit-arguments)
       (list (cons bound term-type))]
      [_ (list)]))

  (define (derive-implicit-argument-types-in-judgement-form
           implicit-arguments
           jf)
    (map (λ (x) (cons x x)) (map canonical-name implicit-arguments))
    #;(match jf
      [`(,_ ,arguments ...)
       (append*
        (for/list ([arg arguments]
                   [arg-type arg-types])
          (match arg
            [`(#:term ,term)
             (derive-implicit-argument-types-in-term
              implicit-arguments
              arg-type
              term)]
            [bound
             #:when (member bound implicit-arguments)
             (list (cons bound arg-type))]
            [_ (list)])))]))

  ; Returns a list of variables that are bound in term-to-find
  ; from the innermost to the outermost
  (define ((find-binding jfs) term-to-find)
    (define (find-binding-in-term already-bound term term-to-find)
      (match term
        [found
         #:when (equal? found term-to-find)
         (list already-bound)]

        [`(#:under-binding ,variable ,term)
         (find-binding-in-term (cons variable already-bound)
                               term
                               term-to-find)]

        [`(#:substitute ,term ,from ,to) '()]

        [application
         #:when (list? application)
         (remove-duplicates
          (append*
           (for/list ([head/arg application])
             (find-binding-in-term
              already-bound
              head/arg
              term-to-find))))]

        [_ '()]))

    (define (find-binding-in-jf jf term-to-find)
      (match jf
        [`(,_ ,arguments ...)
         (remove-duplicates
          (append*
           (for/list ([argument arguments])
             (match argument
               [`(#:term ,term)
                (find-binding-in-term '() term term-to-find)]
               [_ '()]))))]))

    (let ([all-bindings-found
           (remove-duplicates
            (append* (for/list ([jf jfs])
                       (find-binding-in-jf jf term-to-find))))])

      (when (> (length all-bindings-found) 1)
        (error 'define-proposition
               "Multiple occurrences of term '~a' has inconsistent bindings."
               term-to-find))

      (if (empty? all-bindings-found)
          (list #f)
          (first all-bindings-found))))


  (define (translate-constructor constructor)
    (let-values ([(implicit-arguments
                   constructor-name
                   constructor-argument-types
                   constructor-result-type)
                  (match constructor
                    ; define-judgement-form constructors
                    [`(,cn #:implicit ,ia #:result-type ,rt ,cat)
                     (values ia cn cat rt)]

                    ; define-language constructors
                    [`(,cn #:trivial ,cat)
                     (values '() cn cat name)]
                    [`(,cn ,cat)
                     (values '() cn cat name)])])

      (define derived-implicit-argument-types
        (append* (for/list ([jf (list* constructor-result-type
                                       constructor-argument-types)])
                   (derive-implicit-argument-types-in-judgement-form
                    implicit-arguments
                    jf))))

      (parameterize ([find-binding-in-current-definition
                      (find-binding (list* constructor-result-type
                                           constructor-argument-types))])
        (format "~aProp_~a : ~a ~a ~a ~a."
                (encode-identifier (current-language))
                (encode-identifier constructor-name)
                (string-join
                 (for/list ([implicit-argument implicit-arguments]
                            #:when (assoc implicit-argument
                                          derived-implicit-argument-types))
                   (format "{ ~a : ~a }"
                           (encode-identifier implicit-argument)
                           (translate-type
                            '()
                            (cdr (assoc implicit-argument
                                        derived-implicit-argument-types))))))
                (string-join
                 (map ((curry translate-type) implicit-arguments)
                      constructor-argument-types)
                 " → ")
                (if (empty? constructor-argument-types)
                    ""
                    "→")
                (translate-type implicit-arguments
                                constructor-result-type)))))

  (string-append
   proposition-declaration
   "\n\n"
   (string-join (map translate-constructor constructors)
                "\n")
   "\n\n"))


(define-syntax-rule (for-language lang-name stmts ...)
  (parameterize ([current-language lang-name])
    (string-append stmts ...)))

