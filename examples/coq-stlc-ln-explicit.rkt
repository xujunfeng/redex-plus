#lang racket

(require "./../redex-plus.rkt")

(use-backend coq)
(use-binding locally-nameless)

; This language models is used in the proofs for section 7.1


(define-language STLC
  [t ::= nat (-> t t)]
  [e ::= (v variable) (lam (variable : t) e) (app e e) natural]
  [env ::= (cons (variable : t) env) nil]

  #:binding-forms
  (lam (variable : t) e #:refers-to variable)
  )



(define-metafunction STLC
  diff : variable variable -> boolean
  [(diff variable_1 variable_1) #f]
  [(diff variable_1 variable_2) #t])


(define-judgement-form STLC
  #:contract (Value e)
  [(Value natural) "natural-is-value"]
  [(Value (lam (variable : t) e)) "lam-is-value"])


(define-judgement-form STLC
  #:contract (In env variable t)
  [(In (cons (variable : t) env_t) variable t) "axiom"]
  [(In env variable_1 t_1)
   (side-condition (diff variable_1 variable_2))
   --------------------------------------------- "cons"
   (In (cons (variable_2 : t_2) env) variable_1 t_1)])


(define-judgement-form STLC
  #:contract (T env e t)

  [(In env variable t)
   ------------------- "context"
   (T env (v variable) t)]

  [--------- "natural"
   (T env natural nat)]

  [(T (cons (variable_a : t_a) env_t) e_b t_b)
   ------------------------------------------- "abstract"
   (T env_t (lam (variable_a : t_a) e_b) (-> t_a t_b))]

  [(T env e_f (-> t_1 t_2)) (T env e_a t_1)
   --------------------------------------- "apply"
   (T env (app e_f e_a) t_2)]

  )


(define beta-single-step
  (reduction-relation
   STLC
   #:domain e
   [--> (app (lam (variable : t) e_b) e_a) (substitute e_b variable e_a) "subst"]
   ))

(define beta (compatible-closure beta-single-step STLC e))


