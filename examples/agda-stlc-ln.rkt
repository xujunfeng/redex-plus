#lang racket

(require "./../redex-plus.rkt")

(use-backend agda)
(use-binding locally-nameless)


(define-language STLC
  [t ::= nat (-> t t)]
  [e ::= (v variable) (lam (variable : t) e) (app e e) natural]
  [env ::= (cons (variable : t) env) nil]

  #:binding-forms
  (lam (variable : t) e #:refers-to variable)
  )



(define-metafunction STLC
  diff : e variable -> boolean
  [(diff (v variable_1) variable_1) #f]
  [(diff e variable_2) #t])


(define-judgement-form STLC
  #:contract (T env e t)

  [(T env_t e t)
   (side-condition (diff e variable_n))
   ---------------------- "weakening"
   (T (cons (variable_n : t_n) env_t) e t)]

  [(T (cons (variable_e : t) env_t) (v variable_e) t) "axiom"]

  [--------- "natural"
   (T env natural nat)]

  [(T (cons (variable_a : t_a) env_t) e_b t_b)
   ------------------------------------------- "abstract"
   (T env_t (lam (variable_a : t_a) e_b) (-> t_a t_b))]

  [(T env e_f (-> t_1 t_2)) (T env e_a t_1)
   --------------------------------------- "apply"
   (T env (app e_f e_a) t_2)]

  )



(define beta-single-step
  (reduction-relation
   STLC
   #:domain e
   [--> (app (lam (variable : t) e_b) e_a) (substitute e_b variable e_a) "subst"]
   ))

(define beta (compatible-closure beta-single-step STLC e))


