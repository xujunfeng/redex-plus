#lang racket

(require "./../redex-plus.rkt")

(use-backend coq)
(use-binding named)


(define-language F
  [t ::=
     (tvar tv) nat (-> t t)
     (forall tv t)]
  [e ::=
     (var v) (lam (v : t) e) (app e e) natural
     (tlam (tv) e) (tapp e t)]
  [env ::= (cons (v : t) env) (tcons tv env) nil]

  [v ::= variable]
  [tv ::= variable]

  #:binding-forms
  (lam (v : t) e #:refers-to v)
  (tlam (tv) e #:refers-to tv)

  (forall tv t #:refers-to tv)
  )



(define-metafunction F
  diff : v v -> boolean
  [(diff v_1 v_1) #f]
  [(diff v_1 v_2) #t])

(define-metafunction F
  tdiff : tv tv -> boolean
  [(tdiff tv_1 tv_1) #f]
  [(tdiff tv_1 tv_2) #t])


(define-judgement-form F
  #:mode (In I I O)
  #:contract (In env v t)
  [(In (cons (v : t) env) v t) "axiom"]
  [(In env v_1 t_1)
   (side-condition (tdiff v_1 v_2))
   --------------------------------------------- "cons"
   (In (cons (v_2 : t_2) env) v_1 t_1)]
  [(In env v t)
   ------------ "tcons"
   (In (tcons tv env) v t)])

(define-judgement-form F
  #:mode (T I I O)
  #:contract (T env e t)

  [(In env v t)
   ------------------- "context"
   (T env (var v) t)]

  [--------- "natural"
   (T env natural nat)]

  [(T (cons (v_a : t_a) env_t) e_b t_b)
   ------------------------------------------- "abstract"
   (T env_t (lam (v_a : t_a) e_b) (-> t_a t_b))]

  [(T env e_f (-> t_1 t_2)) (T env e_a t_1)
   --------------------------------------- "apply"
   (T env (app e_f e_a) t_2)]


  [(T (tcons tv env) e t)
   ----------------------------------- "Tabstract"
   (T env (tlam (tv) e) (forall tv t))]

  [(T env e (forall tv t_a))
   ------------------------------------------- "Tapply"
   (T env (tapp e t_b) (substitute t_a tv t_b))]

  )


(define beta-single
  (reduction-relation
   F
   #:domain e
   [--> (app (lam (v : t) e_b) e_a) (substitute e_b v e_a) "subst"]
   [--> (tapp (tlam (tv) e) t) (substitute e tv t)         "type-subst"]
   ))

(define beta (compatible-closure beta-single F e))


(define ds
  (build-derivations
   (T (cons (f : (-> nat nat)) nil)
      (app (var f) 10)
      nat)))

(require (only-in redex show-derivations))

(show-derivations ds)

