#lang racket

(require "./../redex-plus.rkt")

(use-backend smt-lib)


(define-language Arith
  [e ::= (add e e) (min e e) (mul e e) integer]
  )


(define eval-single-step
  (reduction-relation
   Arith
   #:domain e
   #:codomain e

   [--> (add integer_1 integer_2)
        (, (+ (term integer_1) (term integer_2)))
        "add"]
   [--> (min integer_1 integer_2)
        (, (- (term integer_1) (term integer_2)))
        "min"]
   [--> (mul integer_a integer_b)
        (, (* (term integer_a) (term integer_b)))
        "mul"]
   ))


(define eval (compatible-closure eval-single-step Arith e))




