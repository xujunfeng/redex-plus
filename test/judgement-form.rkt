#lang racket

; Tests for language

; Testing imports and errors
(require rackunit
         syntax/macro-testing

         "./test-util.rkt"
         (for-syntax "./test-util.rkt")

         "./../redex-plus/warning.rkt"
         )
 
(require "./../redex-plus.rkt")

(displayln "Testing define-judgement-form")


(run-tests
 ;(parameterize
 ;     ([current-warning-handler default-warning-as-error-handler])

 (define-language Nat
   [n (s n) z]
   )

 (define-language Natural
   [n (succ n) zero]
   )

 (check-not-fail
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [----------- "zero"
       (> (s n) z)]

      [(> n_1 n_2)
       ------------------- "more"
       (> (s n_1) (s n_2))]
      )
    (void)))

 (check-not-fail
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [-----------
       (> (s n) z)]

      [(> n_1 n_2)
       -------------------
       (> (s n_1) (s n_2))]
      )
    (void)))

 (check-not-fail
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [----------- "zero"
       (> (s n) z)]
      )
    (void)))

 (check-syntax-fail
  #rx"misspelling"
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [----------- "zero"
       (> (s n1) z)]
      )
    (void)))

 (check-syntax-fail
  #rx"no axiom"
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [(> n_1 n_2)
       ------------------- "more"
       (> (s n_1) (s n_2))]
      )
    (void)))

 (check-syntax-fail
  #rx"no axiom"
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [(> n_1 n_2)
       --------------- "more"
       (> (s n_1) n_2)]
      )
    (void)))

 (check-syntax-fail
  #rx"no axiom"
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [(> n_1 n_2)
       -------------------
       (> (s n_1) (s n_2))]

      [(> n_1 n_2)
       ---------------
       (> (s n_1) n_2)]

      [(> n_1 n_2)
       ---------------
       (> (s (s n_1)) n_2)]
     
      [(> n_1 n_2)
       ---------------
       (> (s (s n_1)) (s n_2))]
      )
    (void)))

 (check-syntax-fail
  #rx"misspelling"
  (let ()
    (define-judgement-form Nat
      #:mode (> I I)
      #:contract (> n n)

      [(> n_1 n_2)
       -------------------
       (> (s n_1) (s n_2))]

      [(> n_1 n_2)
       ---------------
       (> (s n_1) n_2)]

      [(> n_1 n_2)
       ---------------
       (> (s (s n_1)) n_2)]
     
      [(> n_1 n_2)
       ---------------
       (> (s (s n_1)) (s n_2))]

      [-----------
       (> (s n1) z)]
      )
    (void)))

 )
