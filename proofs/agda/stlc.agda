
open import Agda.Builtin.Bool
open import Agda.Builtin.Nat
  renaming (_==_ to _Nat==_)
open import Agda.Builtin.Int
open import Agda.Builtin.String
open import Agda.Builtin.Equality
open import Agda.Builtin.List

open import Data.Bool.Base
  using (_∧_; if_then_else_)
open import Data.List.Relation.Unary.Any as Any
open import Data.String
  hiding (_<_)
  renaming (_==_ to _String==_)


record Sub (A : Set) (B : Set) : Set₂ where
  field
    closed : List String → B → Set
    sub    : String → A → B → B


data STLC-e :  Set
data STLC-t :  Set
data STLC-env :  Set

data STLC-e where
  STLC-v :  String → STLC-e
  STLC-app :  STLC-e → STLC-e → STLC-e
  STLC-e-natural :  Nat → STLC-e
  STLC-lam :  String → STLC-t → STLC-e → STLC-e

data STLC-t where
  STLC-nat :  STLC-t
  STLC--> :  STLC-t → STLC-t → STLC-t

data STLC-env where
  STLC-cons :  String → STLC-t → STLC-env → STLC-env
  STLC-nil :  STLC-env


STLC-env-eqb : STLC-env → STLC-env → Bool
STLC-t-eqb : STLC-t → STLC-t → Bool
STLC-e-eqb : STLC-e → STLC-e → Bool

STLC-env-eqb (STLC-cons a0 a1 a2) (STLC-cons a0' a1' a2') = (_String==_ a0 a0') ∧ (STLC-t-eqb a1 a1') ∧ (STLC-env-eqb a2 a2')
STLC-env-eqb (STLC-nil ) (STLC-nil ) = true
{-# CATCHALL #-}
STLC-env-eqb _ _ = false

STLC-t-eqb (STLC-nat ) (STLC-nat ) = true
STLC-t-eqb (STLC--> a0 a1) (STLC--> a0' a1') = (STLC-t-eqb a0 a0') ∧ (STLC-t-eqb a1 a1')
{-# CATCHALL #-}
STLC-t-eqb _ _ = false

STLC-e-eqb (STLC-v a0) (STLC-v a0') = (_String==_ a0 a0')
STLC-e-eqb (STLC-app a0 a1) (STLC-app a0' a1') = (STLC-e-eqb a0 a0') ∧ (STLC-e-eqb a1 a1')
STLC-e-eqb (STLC-e-natural a0) (STLC-e-natural a0') = (_Nat==_ a0 a0')
STLC-e-eqb (STLC-lam a0 a1 a2) (STLC-lam a0' a1' a2') = (_String==_ a0 a0') ∧ (STLC-t-eqb a1 a1') ∧ (STLC-e-eqb a2 a2')
{-# CATCHALL #-}
STLC-e-eqb _ _ = false


data closed-STLC-e-STLC-e : List String → STLC-e → Set
sub-STLC-e-STLC-e   : String → STLC-e → STLC-e → STLC-e

data closed-STLC-e-STLC-e where
  STLC-v-closed : ∀ {a0 bvs} → Any (a0 ≡_) bvs → closed-STLC-e-STLC-e bvs (STLC-v a0)
  STLC-app-closed : ∀ {a0 a1 bvs} → (closed-STLC-e-STLC-e bvs a0) → (closed-STLC-e-STLC-e bvs a1) → closed-STLC-e-STLC-e bvs (STLC-app a0 a1)
  STLC-e-natural-closed : ∀ {a0 bvs} → closed-STLC-e-STLC-e bvs (STLC-e-natural a0)
  STLC-lam-closed : ∀ {a0 a1 a2 bvs} → (closed-STLC-e-STLC-e (a0 ∷ bvs) a2) → closed-STLC-e-STLC-e bvs (STLC-lam a0 a1 a2)

sub-STLC-e-STLC-e x to o@(STLC-v x') = if x String== x' then to else o
sub-STLC-e-STLC-e x to (STLC-app a0 a1) = STLC-app (sub-STLC-e-STLC-e x to a0) (sub-STLC-e-STLC-e x to a1)
sub-STLC-e-STLC-e x to (STLC-e-natural a0) = STLC-e-natural a0
sub-STLC-e-STLC-e x to (STLC-lam a0 a1 a2) = STLC-lam a0 a1 (if x String== a0 then a2 else (sub-STLC-e-STLC-e x to a2))

instance
  STLC-e-STLC-e-Sub : Sub STLC-e STLC-e
  STLC-e-STLC-e-Sub = record { closed = closed-STLC-e-STLC-e; sub = sub-STLC-e-STLC-e }
STLC-diff : String → String → Bool

STLC-diff t1 t2 = (if ((_String==_ t1 t2)) then (false ) else (true ))



data STLC-Value : STLC-e →  Set

data STLC-Value where
  STLCProp-natural-is-value : ∀ {natural} → (STLC-Value (STLC-e-natural natural))
  STLCProp-lam-is-value : ∀ {`variable t e} → (STLC-Value (STLC-lam `variable t e))

data STLC-In : STLC-env → String → STLC-t →  Set

data STLC-In where
  STLCProp-axiom : ∀ {`variable t env:t} → (STLC-In (STLC-cons `variable t env:t) `variable t)
  STLCProp-cons : ∀ {variable:2 t:2 env variable:1 t:1} → (STLC-In env variable:1 t:1) → ((STLC-diff variable:1 variable:2) ≡ true) → (STLC-In (STLC-cons variable:2 t:2 env) variable:1 t:1)

data STLC-T : STLC-env → STLC-e → STLC-t →  Set

data STLC-T where
  STLCProp-context : ∀ {env `variable t} → (STLC-In env `variable t) → (STLC-T env (STLC-v `variable) t)
  STLCProp-natural : ∀ {env natural} → (STLC-T env (STLC-e-natural natural) (STLC-nat ))
  STLCProp-abstract : ∀ {env:t variable:a t:a e:b t:b} → (STLC-T (STLC-cons variable:a t:a env:t) e:b t:b) → (STLC-T env:t (STLC-lam variable:a t:a e:b) (STLC--> t:a t:b))
  STLCProp-apply : ∀ {env e:f e:a t:2 t:1} → (STLC-T env e:f (STLC--> t:1 t:2)) → (STLC-T env e:a t:1) → (STLC-T env (STLC-app e:f e:a) t:2)

data STLC-beta-single-step : STLC-e → STLC-e →  Set

data STLC-beta-single-step where
  STLCProp-beta-single-step-subst : ∀ {`variable t e:b e:a} → (STLC-beta-single-step (STLC-app (STLC-lam `variable t e:b) e:a) (sub-STLC-e-STLC-e `variable e:a e:b))

data STLC-beta : STLC-e → STLC-e →  Set

data STLC-beta where
  STLCProp-beta-single-step-subst-cc : ∀ {`variable t e:b e:a} → (STLC-beta (STLC-app (STLC-lam `variable t e:b) e:a) (sub-STLC-e-STLC-e `variable e:a e:b))
  STLCProp-beta-cc-lam : ∀ {e:from e:to `variable t:i1} → (STLC-beta e:from e:to) → (STLC-beta (STLC-lam `variable t:i1 e:from) (STLC-lam `variable t:i1 e:to))
  STLCProp-beta-cc-app : ∀ {e:from e:to e:i1} → (STLC-beta e:from e:to) → (STLC-beta (STLC-app e:from e:i1) (STLC-app e:to e:i1))
  STLCProp-beta-cc-app-2 : ∀ {e:from e:to e:i0} → (STLC-beta e:from e:to) → (STLC-beta (STLC-app e:i0 e:from) (STLC-app e:i0 e:to))









{- generated code ends -}

open import Relation.Binary.PropositionalEquality
  using (_≡_; _≢_; refl; sym; ≢-sym; cong; cong₂)
open import Data.String using (String; _≟_)
open import Data.Nat using (ℕ; zero; suc)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Product
  using (_×_; proj₁; proj₂; ∃; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Function using (_∘_)
open import Data.Product using (Σ; _,_; ∃; Σ-syntax; ∃-syntax)
open import Data.Bool.Properties hiding (_≟_)



data _⟶_ : STLC-e → STLC-e →  Set

data _⟶_ where
  β⟶ : ∀ {`variable t e:b e:a} → STLC-Value e:a → ((STLC-app (STLC-lam `variable t e:b) e:a) ⟶ (sub-STLC-e-STLC-e `variable e:a e:b))
  app⟶*l : ∀ {e:from e:to e:i1} → (e:from ⟶ e:to) → ((STLC-app e:from e:i1) ⟶ (STLC-app e:to e:i1))
  app⟶*r : ∀ {e:from e:to e:i0} → STLC-Value e:i0 → (e:from ⟶ e:to) → ((STLC-app e:i0 e:from) ⟶ (STLC-app e:i0 e:to))


{- value does not reduce -}
V-→ : ∀ {e e'} → STLC-Value e → ¬ (e ⟶ e')
V-→ STLCProp-natural-is-value ()
V-→ STLCProp-lam-is-value ()

[_⇒_]_ = sub-STLC-e-STLC-e {- [ v ⇒ e1 ] e2 = sub v e1 e2 -}
_≫_ = STLC-beta
_∈_∷_ = STLC-cons {- v ∈ t ∷ env = (cons (v : t) env) -}
∅ = STLC-nil

_∋_∈_ = STLC-In {- v ∋ t ∈ env = (In v t env) -}
_⊢_∈_ = STLC-T  {- env ⊢ e ∈ t = (T env e t) -}

{- progress -}

prog : ∀ e {t} → ∅ ⊢ e ∈ t → STLC-Value e ⊎ ∃[ e' ] (e ≫ e')
prog (STLC-v x) (STLCProp-context ())
prog (STLC-app e₁ e₂) (STLCProp-apply T₁ T₂) with prog e₁ T₁
... | inj₂ (e₃ , e₁R) = inj₂ (STLC-app e₃ e₂ , STLCProp-beta-cc-app e₁R)
... | inj₁ x with e₁
... | STLC-lam x₁ x₂ x₃ = inj₂ ([ x₁ ⇒ e₂ ] x₃ , STLCProp-beta-single-step-subst-cc)
prog (STLC-e-natural x) V = inj₁ STLCProp-natural-is-value
prog (STLC-lam x x₁ e) V = inj₁ STLCProp-lam-is-value


same-not-diff : ∀ {x} → STLC-diff x x ≢ true
same-not-diff {x} with x ≟ x
... | no Hneq' = ⊥-elim (Hneq' refl)
... | yes refl with false Data.Bool.Properties.≟ true
...   | no Hneq = Hneq

diff-neq : ∀ {x x'} → STLC-diff x x' ≡ true → x ≢ x'
diff-neq {x} {x'} Hdiff with x ≟ x'
diff-neq {x} {x'} () | yes refl
... | no Hneq = Hneq

neq-diff : ∀ {x y} → x ≢ y → STLC-diff x y ≡ true
neq-diff {x} {y} Hneq with x ≟ y
... | yes refl = ⊥-elim (Hneq refl)
... | no Hneq' = refl



extend : ∀ {env env'}
  → (∀ {x t} → env ∋ x ∈ t → env' ∋ x ∈ t)
  → (∀ {x y tx ty} → (y ∈ ty ∷ env) ∋ x ∈ tx → (y ∈ ty ∷ env') ∋ x ∈ tx)
extend HIn STLCProp-axiom = STLCProp-axiom
extend HIn (STLCProp-cons x x₁) = STLCProp-cons (HIn x) x₁

rename : ∀ {env env'}
  → (∀ {v t} → env ∋ v ∈ t → env' ∋ v ∈ t)
  → (∀ {e t} → env ⊢ e ∈ t → env' ⊢ e ∈ t)
rename HIn (STLCProp-context x) = STLCProp-context (HIn x)
rename HIn STLCProp-natural = STLCProp-natural
rename HIn (STLCProp-abstract HT) = STLCProp-abstract (rename (extend HIn) HT)
rename HIn (STLCProp-apply HT HT₁) = STLCProp-apply (rename HIn HT) (rename HIn HT₁)

weaken : ∀ {env e t} → ∅ ⊢ e ∈ t → env ⊢ e ∈ t
weaken {env} HT = rename HIn HT
  where
  HIn : ∀ {v t} → ∅ ∋ v ∈ t → env ∋ v ∈ t
  HIn ()


drop : ∀ {env x e t₁ t₂ t}
  → (x ∈ t₁ ∷ (x ∈ t₂ ∷ env)) ⊢ e ∈ t
  → (x ∈ t₁ ∷ env) ⊢ e ∈ t
drop {env} {x} {e} {t₁} {t₂} HT = rename HIn HT
  where
  HIn : ∀ {x' x't} → (x ∈ t₁ ∷ (x ∈ t₂ ∷ env)) ∋ x' ∈ x't
                   → (x ∈ t₁ ∷ env) ∋ x' ∈ x't
  HIn STLCProp-axiom = STLCProp-axiom
  HIn (STLCProp-cons STLCProp-axiom x₁) = ⊥-elim (same-not-diff x₁)
  HIn (STLCProp-cons (STLCProp-cons HIn' x) x₁) = STLCProp-cons HIn' x


swap : ∀ {env x y tx ty e t}
  → x ≢ y
  → (x ∈ tx ∷ (y ∈ ty ∷ env)) ⊢ e ∈ t
  → (y ∈ ty ∷ (x ∈ tx ∷ env)) ⊢ e ∈ t
swap {env} {x} {y} {tx} {ty} {e} {t} Hneq HT = rename HIn HT
  where
  HIn : ∀ {x' x't} → (x ∈ tx ∷ (y ∈ ty ∷ env)) ∋ x' ∈ x't
                   → (y ∈ ty ∷ (x ∈ tx ∷ env)) ∋ x' ∈ x't
  HIn STLCProp-axiom = STLCProp-cons STLCProp-axiom (neq-diff Hneq)
  HIn (STLCProp-cons STLCProp-axiom x₁) = STLCProp-axiom
  HIn (STLCProp-cons (STLCProp-cons x x₂) x₁) = STLCProp-cons (STLCProp-cons x x₁) x₂

subst : ∀ {env v e to vt t}
  → ∅ ⊢ to ∈ vt
  → (v ∈ vt ∷ env) ⊢ e ∈ t
  → env ⊢ [ v ⇒ to ] e ∈ t
subst {v = v} HTv (STLCProp-context {`variable = x} STLCProp-axiom) with v ≟ x
... | yes refl = weaken HTv
{-... | no Hneq = ⊥-elim (Hneq refl)-}
... | no Hneq = ⊥-elim (Hneq refl)
subst {v = v} HTv (STLCProp-context {`variable = x} (STLCProp-cons HIn' Hdiff)) with v ≟ x
... | yes refl = ⊥-elim (diff-neq Hdiff refl)
... | no Hneq = STLCProp-context HIn'
subst HTv STLCProp-natural = STLCProp-natural
subst {v = v} HTv (STLCProp-abstract {variable:a = x} HTe') with v ≟ x
... | yes refl = STLCProp-abstract (drop HTe')
... | no Hneq = STLCProp-abstract (subst HTv (swap (≢-sym Hneq) HTe'))
subst HTv (STLCProp-apply HTe₁ HTe₂) = STLCProp-apply (subst HTv HTe₁) (subst HTv HTe₂)


pres : ∀ {e t e'}
  → ∅ ⊢ e ∈ t
  → e ⟶ e'
  → ∅ ⊢ e' ∈ t
pres (STLCProp-context x) ()
pres STLCProp-natural ()
pres (STLCProp-abstract x) ()
pres (STLCProp-apply (STLCProp-abstract x) x₁) (β⟶ x₂) =
  subst x₁ x
pres (STLCProp-apply x x₁) (app⟶*l y) = STLCProp-apply (pres x y) x₁
pres (STLCProp-apply x x₁) (app⟶*r x₂ y) = STLCProp-apply x (pres x₁ y)
