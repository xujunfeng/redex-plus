(* Native type imports *)
Require Import Bool String PeanoNat.


(* Extra native type imports *)
Require Import BinInt.

Require Import List.
Open Scope list_scope.


(* Applies a reduction relation to a term t,
   returns the list of all other terms t reduces to. *)
Definition apply_rr {A B} (rrs : list (A -> list B)) t :=
  flat_map (fun rr => rr t) rrs.


Class Sub B A : Type :=
{ closed : list string -> A -> Prop;
  sub    : string -> B -> A -> A;
}.




Inductive STLC_e : Type :=
| STLC_v : string -> STLC_e
| STLC_app : STLC_e -> STLC_e -> STLC_e
| STLC_e_natural : nat -> STLC_e
| STLC_lam : string -> STLC_t -> STLC_e -> STLC_e
with STLC_t : Type :=
| STLC_nat : STLC_t
| STLC___arrow__ : STLC_t -> STLC_t -> STLC_t
with STLC_env : Type :=
| STLC_cons : string -> STLC_t -> STLC_env -> STLC_env
| STLC_nil : STLC_env.

Fixpoint STLC_env_eqb  (t1 t2 : STLC_env) : bool :=
  match t1, t2 with
  | STLC_cons a0 a1 a2, STLC_cons a0' a1' a2' => (String.eqb a0 a0') && (STLC_t_eqb a1 a1') && (STLC_env_eqb a2 a2')
  | STLC_nil , STLC_nil  => true
  | _, _ => false
  end
with STLC_t_eqb  (t1 t2 : STLC_t) : bool :=
  match t1, t2 with
  | STLC_nat , STLC_nat  => true
  | STLC___arrow__ a0 a1, STLC___arrow__ a0' a1' => (STLC_t_eqb a0 a0') && (STLC_t_eqb a1 a1')
  | _, _ => false
  end
with STLC_e_eqb  (t1 t2 : STLC_e) : bool :=
  match t1, t2 with
  | STLC_v a0, STLC_v a0' => (String.eqb a0 a0')
  | STLC_app a0 a1, STLC_app a0' a1' => (STLC_e_eqb a0 a0') && (STLC_e_eqb a1 a1')
  | STLC_e_natural a0, STLC_e_natural a0' => (Nat.eqb a0 a0')
  | STLC_lam a0 a1 a2, STLC_lam a0' a1' a2' => (String.eqb a0 a0') && (STLC_t_eqb a1 a1') && (STLC_e_eqb a2 a2')
  | _, _ => false
  end.

Inductive closed_STLC_e_in_STLC_e : list string -> STLC_e -> Prop :=
| STLC_v_closed {bvs a0} : In a0 bvs -> closed_STLC_e_in_STLC_e bvs (STLC_v a0)
| STLC_app_closed {bvs a0 a1} : (closed_STLC_e_in_STLC_e bvs a0) -> (closed_STLC_e_in_STLC_e bvs a1) -> closed_STLC_e_in_STLC_e bvs (STLC_app a0 a1)
| STLC_e_natural_closed {bvs a0} : closed_STLC_e_in_STLC_e bvs (STLC_e_natural a0)
| STLC_lam_closed {bvs a0 a1 a2} : (closed_STLC_e_in_STLC_e (cons a0 bvs) a2) -> closed_STLC_e_in_STLC_e bvs (STLC_lam a0 a1 a2)
.

Fixpoint sub_STLC_e_in_STLC_e x to e :=
match e with
  | STLC_v x' => if String.eqb x x' then to else e
  | STLC_app a0 a1 => STLC_app (sub_STLC_e_in_STLC_e x to a0) (sub_STLC_e_in_STLC_e x to a1)
  | STLC_e_natural a0 => STLC_e_natural a0
  | STLC_lam a0 a1 a2 => STLC_lam a0 a1 (if String.eqb x a0 then a2 else (sub_STLC_e_in_STLC_e x to a2))
end
.

Instance Sub_STLC_e_in_STLC_e : Sub STLC_e STLC_e :=
{
  closed := closed_STLC_e_in_STLC_e;
  sub    :=   sub_STLC_e_in_STLC_e;
}.

Definition STLC_diff  (t1 : string) (t2 : string) : bool :=
  (if (((String.eqb t1 t2))) then ((false )) else ((true ))).



Inductive STLC_Value : STLC_e -> Prop :=
| STLCProp_natural_is_value {natural} : (STLC_Value (STLC_e_natural natural))
| STLCProp_lam_is_value {variable t e} : (STLC_Value (STLC_lam variable t e)).
Ltac autoderive_STLC_Value :=
(apply STLCProp_natural_is_value; autoderive_STLC_Value) +
(apply STLCProp_lam_is_value; autoderive_STLC_Value) +
(progress auto; autoderive_STLC_Value) +
(progress compute; autoderive_STLC_Value).

Ltac autoderive := first [
autoderive_STLC_Value].



Inductive STLC_In : STLC_env -> string -> STLC_t -> Prop :=
| STLCProp_axiom {variable t env__t} : (STLC_In (STLC_cons variable t env__t) variable t)
| STLCProp_cons {variable__2 t__2 env variable__1 t__1} : (STLC_In env variable__1 t__1) -> (Is_true (STLC_diff variable__1 variable__2)) -> (STLC_In (STLC_cons variable__2 t__2 env) variable__1 t__1).
Ltac autoderive_STLC_In :=
(apply STLCProp_axiom; autoderive_STLC_In) +
(apply STLCProp_cons; autoderive_STLC_In) +
(progress auto; autoderive_STLC_In) +
(progress compute; autoderive_STLC_In).

Ltac autoderive ::= first [
autoderive_STLC_In |
autoderive_STLC_Value].



Inductive STLC_T : STLC_env -> STLC_e -> STLC_t -> Prop :=
| STLCProp_context {env variable t} : (STLC_In env variable t) -> (STLC_T env (STLC_v variable) t)
| STLCProp_natural {env natural} : (STLC_T env (STLC_e_natural natural) (STLC_nat ))
| STLCProp_abstract {env__t variable__a t__a e__b t__b} : (STLC_T (STLC_cons variable__a t__a env__t) e__b t__b) -> (STLC_T env__t (STLC_lam variable__a t__a e__b) (STLC___arrow__ t__a t__b))
| STLCProp_apply {env e__f e__a t__2 t__1} : (STLC_T env e__f (STLC___arrow__ t__1 t__2)) -> (STLC_T env e__a t__1) -> (STLC_T env (STLC_app e__f e__a) t__2).
Ltac autoderive_STLC_T :=
(apply STLCProp_context; autoderive_STLC_T) +
(apply STLCProp_natural; autoderive_STLC_T) +
(apply STLCProp_abstract; autoderive_STLC_T) +
(apply STLCProp_apply; autoderive_STLC_T) +
(progress auto; autoderive_STLC_T) +
(progress compute; autoderive_STLC_T).

Ltac autoderive ::= first [
autoderive_STLC_T |
autoderive_STLC_In |
autoderive_STLC_Value].



Inductive STLC_beta_single_step : STLC_e -> STLC_e -> Prop :=
| STLCProp_beta_single_step_subst {variable t e__b e__a} : (STLC_beta_single_step (STLC_app (STLC_lam variable t e__b) e__a) (sub variable e__a e__b)).
Ltac autoderive_STLC_beta_single_step :=
(apply STLCProp_beta_single_step_subst; autoderive_STLC_beta_single_step) +
(progress auto; autoderive_STLC_beta_single_step) +
(progress compute; autoderive_STLC_beta_single_step).

Ltac autoderive ::= first [
autoderive_STLC_beta_single_step |
autoderive_STLC_T |
autoderive_STLC_In |
autoderive_STLC_Value].



Inductive STLC_beta : STLC_e -> STLC_e -> Prop :=
| STLCProp_beta_single_step_subst_cc {variable t e__b e__a} : (STLC_beta (STLC_app (STLC_lam variable t e__b) e__a) (sub variable e__a e__b))
| STLCProp_beta_cc_lam {e__from e__to variable t__i1} : (STLC_beta e__from e__to) -> (STLC_beta (STLC_lam variable t__i1 e__from) (STLC_lam variable t__i1 e__to))
| STLCProp_beta_cc_app {e__from e__to e__i1} : (STLC_beta e__from e__to) -> (STLC_beta (STLC_app e__from e__i1) (STLC_app e__to e__i1))
| STLCProp_beta_cc_app_2 {e__from e__to e__i0} : (STLC_beta e__from e__to) -> (STLC_beta (STLC_app e__i0 e__from) (STLC_app e__i0 e__to)).
Ltac autoderive_STLC_beta :=
(apply STLCProp_beta_single_step_subst_cc; autoderive_STLC_beta) +
(apply STLCProp_beta_cc_lam; autoderive_STLC_beta) +
(apply STLCProp_beta_cc_app; autoderive_STLC_beta) +
(apply STLCProp_beta_cc_app_2; autoderive_STLC_beta) +
(progress auto; autoderive_STLC_beta) +
(progress compute; autoderive_STLC_beta).

Ltac autoderive ::= first [
autoderive_STLC_beta |
autoderive_STLC_beta_single_step |
autoderive_STLC_T |
autoderive_STLC_In |
autoderive_STLC_Value].






(* generated code ends here *)




Notation "e >-> e'" := (STLC_beta e e') (at level 20).
Notation "env |- e : t" := (STLC_T env e t)
  (at level 20, e at next level, t at next level).
Notation "x : t :: env" := (STLC_cons x t env)
  (at level 20, t at next level, env at next level).
Notation "x : t <- env" := (STLC_In env x t)
  (at level 20, t at next level, env at next level).


Theorem Prog : forall e t, STLC_nil |- e : t -> STLC_Value e \/ exists e', e >-> e'.
Proof with eauto.
  intros e t HT.

  remember STLC_nil as env0.
  generalize dependent Heqenv0.

  induction HT; intros Heqenv0.
  - inversion H; subst.
    + inversion H0.
    + inversion H2.
  - left. autoderive.
  - left. autoderive.
  - right.
    destruct IHHT1; subst...
    + destruct H; inversion HT1; subst.
      exists (sub variable e__a e).
      autoderive.
    + inversion H.
      exists (STLC_app x e__a).
      autoderive.
Qed.

Lemma same_not_diff : forall v, ~ Is_true (STLC_diff v v).
Proof with eauto.
  intros v.
  unfold STLC_diff.
  rewrite String.eqb_refl...
Qed.

Lemma neq_diff : forall v v', v <> v' <-> Is_true (STLC_diff v v').
Proof with eauto.
  intros v v'.
  unfold STLC_diff.
  split.
  - intros Hneq.
    apply String.eqb_neq in Hneq.
    rewrite Hneq.
    unfold Is_true...
  - intros Hd.
    destruct (String.string_dec v v')...
    contradict Hd.
    rewrite e.
    rewrite String.eqb_refl...
Qed.


Lemma Extend : forall env env',
    (forall x t, x : t <- env -> x : t <- env') ->
    (forall x y tx ty, x : tx <- (y : ty :: env) -> x : tx <- (y : ty :: env')).
Proof using Type with eauto.
  intros env env' HIn x y tx ty HxIn.
  inversion HxIn; autoderive.
Qed.


Lemma Rename : forall env env',
  (forall v t, v : t <- env -> v : t <- env') ->
  (forall e t, env |- e : t -> env' |- e : t).
Proof using Type with eauto.
  intros env env' HIn e t HT.

  generalize dependent env'.

  induction HT; intros env' HIn.
  - apply STLCProp_context.
    apply HIn...
  - autoderive.
  - apply STLCProp_abstract.
    apply IHHT.
    intros v t.
    apply Extend...
  - eapply STLCProp_apply...
Qed.


Corollary Weaken : forall env e t, STLC_nil |- e : t -> env |- e : t.
Proof with eauto.
  intros env e t HT0.
  apply (Rename STLC_nil env)...
  intros v t0 Hnil.
  inversion Hnil.
Qed.

Corollary Drop : forall env x e t1 t2 t,
    (x : t1 :: (x : t2 :: env) |- e : t) -> (x : t1 :: env |- e : t).
Proof with eauto.
  intros env x e t1 t2 t HT.
  apply (Rename (x : t1 :: (x : t2 :: env)))...
  intros v t0 HIn.
  destruct (string_dec x v); subst.
  - inversion HIn; subst.
    + autoderive.
    + apply same_not_diff in H5.
      contradiction.
  - apply STLCProp_cons.
    + inversion HIn; subst.
      * contradiction.
      * inversion H4; subst...
        contradiction.
    + apply neq_diff...
Qed.

Corollary Swap : forall env x y e t1 t2 t,
    x <> y ->
    (x : t1 :: (y : t2 :: env)) |- e : t ->
    (y : t2 :: (x : t1 :: env)) |- e : t.
Proof with eauto.
  intros env x y e t1 t2 t Hdiff HT.
  apply (Rename (x : t1 :: (y : t2 :: env)))...
  intros v t0 HIn.
  inversion HIn; subst.
  - eapply STLCProp_cons.
    + autoderive.
    + apply neq_diff...
  - inversion H4; subst; autoderive...
Qed.



Lemma Subst :
  forall env x xt v e t,
     STLC_nil |- v : xt
  -> x : xt :: env |- e : t
  -> env |- sub x v e : t.
Proof with eauto.
  intros env x xt v e t HTv HTe.

  generalize dependent x.
  generalize dependent xt.
  generalize dependent env.
  generalize dependent t.

  induction e; intros t env xt HTv x HTe.
  - inversion HTe. subst.
    simpl sub.
    destruct (String.string_dec x s).
    + rewrite <- String.eqb_eq in e.
      rewrite e.
      apply Weaken.
      inversion H1; subst...
      rewrite String.eqb_eq in e.
      subst.
      contradict H6.
      apply same_not_diff.
    + rewrite <- String.eqb_neq in n.
      rewrite n.
      apply STLCProp_context.
      inversion H1; subst...
      rewrite String.eqb_refl in n.
      inversion n.
  - inversion HTe.
    simpl sub.
    eapply STLCProp_apply...
  - inversion HTe.
    autoderive.
  - inversion HTe. subst.
    destruct (String.string_dec x s); simpl.
    + rewrite <- String.eqb_eq in e0.
      rewrite e0.
      apply STLCProp_abstract.
      eapply Drop.
      rewrite String.eqb_eq in e0.
      subst...
    + apply STLCProp_abstract.
      apply Swap in H4...
      rewrite <- String.eqb_neq in n.
      rewrite n...
Qed.



(* redefine beta *)
Inductive beta : STLC_e -> STLC_e -> Prop :=
| beta_single_step_subst_cc {variable t e__b e__a} : (beta (STLC_app (STLC_lam variable t e__b) e__a) (sub variable e__a e__b))
| beta_cc_app {e__from e__to e__i1} : (beta e__from e__to) -> (beta (STLC_app e__from e__i1) (STLC_app e__to e__i1))
| beta_cc_app_2 {e__from e__to e__i0} : (beta e__from e__to) -> (beta (STLC_app e__i0 e__from) (STLC_app e__i0 e__to)).

Notation "e --> e'" := (beta e e') (at level 20).

Theorem Pres : forall e t e', STLC_nil |- e : t -> e --> e' -> STLC_nil |- e' : t.
Proof with eauto.
  intros e t e' HT HR.
  remember STLC_nil as env0.
  generalize dependent e'.

  induction HT; intros e' HR;
    inversion HR; subst; try eapply STLCProp_apply...
  eapply Subst...
  inversion HT1...
Qed.



