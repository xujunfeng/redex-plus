# redex-plus

Redex-plus (name is not final) is an extension of PLT Redex.
Currently, it does the following:

- Generate Coq type definitions for language definitions.
- Checking language and judgement form definitions for well-foundedness.
- Detecting potential misspellings in the definitions.

Redex-plus supports binding form specifications for language definitions.
When generating Coq definitions, binding well be implemented using
the shifted-names ([GitHub](https://github.com/lpw25/shifted-names)) library.
To use the generated Coq definitions, make sure that you can run
`Require Import String Morph Var Context Relative.` in your Coq setup.
Having `-R <path-to-shifted-names-directory>/src SNames`
in the `_CoqProject` file would be sufficient.

