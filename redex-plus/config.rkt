#lang racket

(provide backend-name
         use-backend

         (rename-out [get-binding-name binding-name])
         use-binding

         (rename-out [get-reduction-relation-representation
                      reduction-relation-representation])
         use-reduction-relation-representation

         typed-variables?
         use-typed-variables

         binding-available?
         typed-variables-available?
         reduction-relation-representation-available?

         shift-free-variables?
         )


(define backend-name (make-parameter 'redex))
(define-syntax-rule (use-backend name)
  (backend-name 'name))

(define binding-name (make-parameter 'default))
(define-syntax-rule (use-binding name)
  (binding-name 'name))

(define typed-variables? (make-parameter #f))
(define-syntax-rule (use-typed-variables)
  (typed-variables? #t))

(define (get-binding-name)
  (if (equal? (binding-name) 'default)
      (match (backend-name)
        ['redex  'named]
        ['smt-lib 'concrete-int]
        ['coq    'shifted-names]
        ['beluga 'hoas]
        ['agda   'locally-nameless]
        [_ #f])
      (binding-name)))

(define (binding-available? binding backend)
  (match (list binding backend)
    [`(named beluga) #f]
    [`(named ,_) #t]

    [`(concrete-int smt-lib) #t]

    ['(hoas beluga) #t]

    ['(locally-nameless agda) #t]
    ['(locally-nameless coq) #t]

    ['(shifted-names coq) #t]

    [_ #f]
    ))

(define (typed-variables-available? binding backend)
  (match (list binding backend)
    ['(locally-nameless coq) #t]

    [_ #f]
    ))


(define reduction-relation-representation (make-parameter 'relational))
(define-syntax-rule (use-reduction-relation-representation name)
  (reduction-relation-representation 'name))

(define (get-reduction-relation-representation)
  ; Just in case a "default" is added in the future
  (reduction-relation-representation))

(define (reduction-relation-representation-available? binding backend)
  (match (list binding backend)
    ['(relational coq) #t]
    ['(relational agda) #t]
    ['(relational smt-lib) #t]

    ['(functional coq) #t]

    [_ #f]))


; If true, then free variables in non-binding positions
; will be shifted so that variables with the same name bound
; at different levels will be differentiated even when they are set free.

; I have turned this off by default
; as it judgement cases etc. with complicated binding levels
; are not handled properly.

(define shift-free-variables? (make-parameter #f))

