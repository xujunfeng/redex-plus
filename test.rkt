#lang racket

(module+ test
  (require "./redex-plus.rkt")
  
  (require "./test/language.rkt"
           "./test/judgement-form.rkt"
           )
  
  ; For Poultry tests use Coq as backend
  (use-backend coq)
  (require "./test/poultry.rkt"
           )
  )
